/************************************************************************
 * Copyright (C) 2020 by Parsa Mousavi                                  *
 * sepamou@gmail.com                                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <https://www.gnu.org/licenses/>*
*************************************************************************/
import QtQuick 2.0
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.private.batpercindicator 1.0

PlasmaComponents.Label {
    BatPerc{
        id:bp
    }
    Text{
        id:perc
        text : bp.get() + "%"
        color : theme.textColor
        renderType: Text.NativeRendering
        fontSizeMode: Text.Fit
        minimumPixelSize: 100
        verticalAlignment: Text.AlignVCenter
        width: parent.width
        height: parent.height
        font.pixelSize: 0.03 * bp.get_scale_factor()
    }
    Timer{
        interval : 3000 ; running : true ; repeat : true
        onTriggered: perc.text = bp.get() + "%" 
    }

    // Always display the full view. Never show the compact icon view
    // like it does by default when shown in the panel.
    Plasmoid.preferredRepresentation: Plasmoid.fullRepresentation
}
