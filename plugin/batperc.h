/************************************************************************
 * Copyright (C) 2020 by Parsa Mousavi                                  *
 * sepamou@gmail.com                                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <https://www.gnu.org/licenses/>*
*************************************************************************/

#ifndef BATPERC_H
#define BATPERC_H

#include <QQuickItem>
#include <QProcess>
#include <QObject>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <QFloat16>
#ifdef HAS_FS_SPEC
#include <filesystem>
namespace fs = std::filesystem;
#else
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#endif


class BatPerc : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(BatPerc)

public:
    explicit BatPerc(QQuickItem *parent = nullptr);
    ~BatPerc() override;
    Q_INVOKABLE QString get();
    Q_INVOKABLE QString get_scale_factor();
protected:
    std::string m_path;
    QString m_temp;
    std::ifstream m_batpercpath;
    float scale_factor;

private:
    void fetch_batperc_file(const std::string&);
    const std::string BAT_PERC_SUPER_DIR = "/sys/devices/";
};

#endif // BATPERC_H
