/************************************************************************
 * Copyright (C) 2020 by Parsa Mousavi                                  *
 * sepamou@gmail.com                                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <https://www.gnu.org/licenses/>*
*************************************************************************/

#include "batperc.h"
#include <QDebug>
#include <QtGlobal>
#include <QScreen>
BatPerc::BatPerc(QQuickItem *parent)
    : QQuickItem(parent)
{
    fetch_batperc_file(BAT_PERC_SUPER_DIR);
    size_t pos;
    if((pos=m_path.find("\n")) != std::string::npos){
        m_path.replace(pos , 1 , "\0");
    }
    try {
        m_batpercpath.open(m_path);
    } catch (std::exception& e) {
        std::cout << e.what() << std::endl;
    }
    std::cout << m_path << std::endl;
    Q_ASSERT(m_batpercpath.is_open());
}

BatPerc::~BatPerc()
{

}

QString BatPerc::get(){
    m_batpercpath.clear();
    m_batpercpath.seekg(0 , m_batpercpath.beg);
    std::string m_temp_str;
    m_batpercpath >> m_temp_str;
    std::cout << m_temp_str << std::endl;
    return QString::fromUtf8(m_temp_str.c_str());
}



void BatPerc::fetch_batperc_file(const std::string &path){
        try
        {
            for (const fs::directory_entry &p : fs::recursive_directory_iterator(path))
            {
                if (p.path().u8string().find("BAT0/capacity") != std::string::npos && p.path().u8string().find("BAT0/capacity_level") == std::string::npos)
                {
//                    m_path = QString::fromUtf8(p.path().u8string().c_str());
                      m_path = p.path().u8string();
                      std::cout << m_path << std::endl;
                }
            }
        }
        catch (fs::filesystem_error &e)
        {
            qDebug() << e.what() << endl;
        }
}

QString BatPerc::get_scale_factor(){
    QSize size = qApp->screens()[0]->size();
    return QString::number(size.height());
}
