/************************************************************************
 * Copyright (C) 2020 by Parsa Mousavi                                  *
 * sepamou@gmail.com                                                    *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <https://www.gnu.org/licenses/>*
*************************************************************************/

#ifndef BATPERCINDICATOR_PLUGIN_H
#define BATPERCINDICATOR_PLUGIN_H

#include <QQmlExtensionPlugin>

class BatpercindicatorPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // BATPERCINDICATOR_PLUGIN_H
