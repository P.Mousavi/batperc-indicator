TEMPLATE = lib
TARGET = batpercindicator
QT += qml quick widgets
CONFIG += plugin

VERSION = $$system(g++ --version)
contains( VERSION, 6.[0-9].[0-9] ) {
    message(g++ 6.x version)
    unix:LIBS += -lstdc++fs
}
contains( VERSION, 7.[0-9].[0-9] ) {
        message(g++ 7.x version)
        unix:LIBS += -lstdc++fs
    }
contains( VERSION, 8.[0-9].[0-9] ) {
        message(g++ 8.x version)
        DEFINES += HAS_FS_SPEC
        CONFIG += c++17
}
contains( VERSION, 9.[0-9].[0-9] ) {
        message(g++ 9.x version)
        DEFINES += HAS_FS_SPEC
        CONFIG += c++17
}
contains( VERSION, 10.[0-9].[0-9] ) {
        message(g++ 10.x version)
        DEFINES += HAS_FS_SPEC
        CONFIG += c++17
}

TARGET = $$qtLibraryTarget($$TARGET)
uri = org.kde.plasma.private.batpercindicator

# Input
SOURCES += \
        batpercindicator_plugin.cpp \
        batperc.cpp


HEADERS += \
        batpercindicator_plugin.h \
        batperc.h

DISTFILES = qmldir

!equals(_PRO_FILE_PWD_, $$OUT_PWD) {
    copy_qmldir.target = $$OUT_PWD/qmldir
    copy_qmldir.depends = $$_PRO_FILE_PWD_/qmldir
    copy_qmldir.commands = $(COPY_FILE) "$$replace(copy_qmldir.depends, /, $$QMAKE_DIR_SEP)" "$$replace(copy_qmldir.target, /, $$QMAKE_DIR_SEP)"
    QMAKE_EXTRA_TARGETS += copy_qmldir
    PRE_TARGETDEPS += $$copy_qmldir.target
}

qmldir.files = qmldir
unix {
    installPath = $$[QT_INSTALL_QML]/$$replace(uri, \., /)
    qmldir.path = $$installPath
    target.path = $$installPath
    plasmoid.path = /usr/share/plasma/plasmoids/com.gitlab.p-mousavi.batpercindicator/contents
    plasmoid.files = ../contents/*
    metadata.path = /usr/share/plasma/plasmoids/com.gitlab.p-mousavi.batpercindicator/
    metadata.files = ../metadata.desktop
    INSTALLS += target qmldir plasmoid metadata
}
