# Battery percentage indicator plasmoid for KDE Plasma5
---
An applet made for KDE Plasma to indicate the battery percentage using /sys interface instead of UPower

# Requirements
---
You need g++ 6.x or above , **make**(GNU make system) ,**QMake**(the default makefile generator for Qt) and Qt development files and libraries.And also you need some plasma qml files and plugins.It's diffucult to mention the exact requirements and dependencies but installing the dependencies for **plasma-workspace** would install almost all(if not all) the required dependencies.

In debian-based systems add the **deb-src** sources into the file `/etc/apt/sources.list` (in ubuntu for example you can do this via graphical programs like **software-sources-gtk** or **software-sources-qt**). Then run :
```
sudo apt update
sudo apt build-dep plasma-workspace
```
# Installation
---
Create a directory named **build**(can be anything else) in the projects root directory then run **qmake** to generate the makefiles based on the configuration of your system and then run **sudo make install** :
```
git clone https://gitlab.com/P.Mousavi/batperc-indicator.git
cd batperc-indicator
mkdir build
cd build
qmake ../plugin/
make
sudo make install
```
If the compilation was successful, you should be able to see the plasmoid **Battery percentage indicator** in the **widgets** side-bar in KDE Plasma.

# Maintainer
---
* Parsa Mousavi - sepamou@gmail.com
